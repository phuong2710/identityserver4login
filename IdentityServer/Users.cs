﻿using IdentityModel;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer
{
    public class Users
    {
        public static List<TestUser> Get()
        {
            return new List<TestUser>
            {

                //Tạo người dùng
                    new TestUser
                    {
                        SubjectId = "1",
                        Username = "admin",
                        Password = "admin",
                        Claims = new List<Claim>
                        {
                            new Claim("roleType", "admin")
                        }
                    },
                    new TestUser
                    {
                        SubjectId = "2",
                        Username = "user",
                        Password = "user",
                        Claims = new List<Claim>
                        {
                            new Claim("roleType", "user")
                        }
                    }

            };
        }
    }
}

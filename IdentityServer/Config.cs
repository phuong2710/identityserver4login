﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;
using System.Security.Claims;

namespace IdentityServer
{
    public class Config
    {


        //Khai báo phạm vi apiscope

        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("api1","My Api")
            };
        }


        //Khai báo tài nguyên apiresource
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("myapiresource","MY API Resource")
                {
                    Scopes = { "api1" }
                }
            };
        }
        //public static IEnumerable<ApiResource> GetApiResources()
        //{
        //    return new List<ApiResource>
        //    {
        //        new ApiResource("myresourceapi", "My Resource API")
        //        {
        //            Scopes = {new Scope("apiscope")}
        //        }
        //    };
        //}


        //Khai báo thông tin máy khách Client
        public static IEnumerable<Client> GetClients =>
            new List<Client>
            {


                new Client
                {
                    ClientId = "client",
                    AllowedGrantTypes = GrantTypes.ClientCredentials, 

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    AllowedScopes = { "api1"}
                },


                //login
                new Client
                {
                    ClientId = "client2",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    AllowedScopes = { "api1"}
                }

            };

        
    }
   
}


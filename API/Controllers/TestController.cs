﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        [Authorize(Policy = "PublicSecure")]
        [HttpGet("public")]
        public ActionResult DisplayPublicData()
        {
            return Ok("This is PUBLIC data");
        }


        [Authorize(Policy = "AdminSecure")]
        [HttpGet("admin")]
        public ActionResult DisplayAdminData()
        {
            return Ok("This is ADMIN data");
        }


        [Authorize(Policy = "UserSecure")]
        [HttpGet("user")]
        public ActionResult DisplayGuestData()
        {
            return Ok("This is USER data");
        }
    }
}
